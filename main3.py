from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
import time

driver = webdriver.Firefox()
driver.get("https://wiki.python.org/moin/FrontPage")

search_box = driver.find_element_by_id("searchinput")
search_box.clear()
search_box.send_keys("Beginner")
time.sleep(2)
submit_button = driver.find_element_by_name("fullsearch")
submit_button.submit()
time.sleep(2)
selectbox = Select(driver.find_element_by_xpath("/html/body/div[2]/div[3]/ul/li[5]/form/div/select"))
selectbox.select_by_visible_text("Raw Text")
time.sleep(5)

driver.close()